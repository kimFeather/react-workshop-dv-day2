/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import App from './App';
import Mainpage from './Mainpage';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Mainpage);
