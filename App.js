/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TextInput, Button } from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  state={
    userName : '',
    passWord : ''
  }

  render() {
    return (

      <View style={styles.container}>

        <View style={styles.content}>
        
          <View style={styles.proImg}>
            <Text style={styles.headerText}>Image</Text>
          </View>
          
          <Text style={styles.headerText}>{this.state.userName}-{this.state.passWord}</Text>

          <View style={styles.box}>
            <TextInput style={styles.headerText} placeholder="Username"
              onChangeText={(userName) => this.setState({ userName })}
            />
          </View>

          <View style={styles.box}>
            <TextInput style={styles.headerText} placeholder="Password"
              onChangeText={(passWord) => this.setState({ passWord })}
            />
          </View>

          <View style={styles.boxTouch}>
            <Button
              onPress={this._onPressButton}
              title="Login"

              onPress={() => {
                Alert.alert('You tapped the button!');
              }}
            />
          </View>

        </View>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'steelblue',
  },
  content: {
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: 'steelblue',
    flex: 1,
  },
  box: {
    backgroundColor: 'powderblue',
    width: 250,
    height: 50,
    margin: 14
  },
  boxTouch: {
    backgroundColor: 'skyblue',
    width: 250,
    height: 50,
    margin: 14,

  },
  header: {
    alignItems: 'center',
    backgroundColor: 'skyblue',
  },
  headerText: {
    alignItems: 'center',
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    padding: 10,
  },
  proImg: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'powderblue',
    borderRadius: 150,
    width: 200,
    height: 200,
    margin: 40
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  }

});
